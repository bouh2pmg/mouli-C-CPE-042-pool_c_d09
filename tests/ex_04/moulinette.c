// No Header
// 03/05/2017 - 16:54:57

#include "moulinette.h"

void	my_swap(int *a, int *b)
{
  int	tmp;

  tmp = *a;
  *a = *b;
  *b = tmp;
}

void	shuffle_the_square(int **tab, int loops)
{
  int	rdm[4];

  while (loops >= 0)
    {
      rdm[0] = random() % 4;
      rdm[1] = random() % 4;
      rdm[2] = random() % 4;
      rdm[3] = random() % 4;
      my_swap(&tab[rdm[0]][rdm[2]], &tab[rdm[1]][rdm[3]]);
      loops--;
    }
}

void	*xmalloc(size_t s)
{
  void	*data;
  
  if ((data = malloc(s)) == NULL)
    exit(-127);
  return (data);
}

int	main()
{
  int	**tab = xmalloc(4 * sizeof(int*));
  tab[0] = xmalloc(4 * sizeof(int));
  tab[0][0] = 0;
  tab[0][1] = 0;
  tab[0][2] = 1;
  tab[0][3] = 1;
  tab[1] = xmalloc(4 * sizeof(int));
  tab[1][0] = 0;
  tab[1][1] = 0;
  tab[1][2] = 1;
  tab[1][3] = 1;
  tab[2] = xmalloc(4 * sizeof(int));
  tab[2][0] = 2;
  tab[2][1] = 2;
  tab[2][2] = 3;
  tab[2][3] = 3;
  tab[3] = xmalloc(4 * sizeof(int));
  tab[3][0] = 2;
  tab[3][1] = 2;
  tab[3][2] = 3;
  tab[3][3] = 3;

  if (check_square(tab) == 0)
    printf("Check True Square ... Ok\n");
  srandom(0x4d3AdBe3);
  shuffle_the_square(tab, 150);
  if (check_square(tab) == 1)
    printf("Check False Square ... Ok\n");
  print_tab(tab);  

  algo_line(tab, 0);
  algo_line(tab, 1);
  algo_line(tab, 2);
  algo_line(tab, 3);

  print_tab(tab);  

  algo_column(tab, 0);
  algo_column(tab, 1);
  algo_column(tab, 2);
  algo_column(tab, 3);

  print_tab(tab);  

  algo_square(tab, 0);
  algo_square(tab, 1);
  algo_square(tab, 2);
  algo_square(tab, 3);

  print_tab(tab);  

  algo_line_reverse(tab, 0);
  algo_line_reverse(tab, 1);
  algo_line_reverse(tab, 2);
  algo_line_reverse(tab, 3);

  print_tab(tab);  

  algo_column_reverse(tab, 0);
  algo_column_reverse(tab, 1);
  algo_column_reverse(tab, 2);
  algo_column_reverse(tab, 3);

  print_tab(tab);  

  algo_square_reverse(tab, 0);
  algo_square_reverse(tab, 1);
  algo_square_reverse(tab, 2);
  algo_square_reverse(tab, 3);

  print_tab(tab);  

  printf("Good is_in_line : %d\n", is_in_line(tab, 0, 2));
  printf("Good is_in_col : %d\n", is_in_col(tab, 0, 2));

  printf("Good is_in_line : %d\n", is_in_line(tab, 3, 2));
  printf("Good is_in_col : %d\n", is_in_col(tab, 3, 2));

  printf("False is_in_line : %d\n", is_in_line(tab, 0, 0));
  printf("False is_in_col : %d\n", is_in_col(tab, 0, 3));

  printf("False is_in_line : %d\n", is_in_line(tab, 3, 0));
  printf("False is_in_col : %d\n", is_in_col(tab, 2, 2));

  return (0);
}

/*
-----------------
| 2 | 3 | 1 | 2 |
-----------------
| 0 | 0 | 0 | 1 |
-----------------
| 1 | 2 | 3 | 0 |
-----------------
| 2 | 3 | 1 | 3 |
-----------------
*/
